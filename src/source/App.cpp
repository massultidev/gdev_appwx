/* Copyright by: P.J. Grochowski */

#include <string>

#include <wx/intl.h>
#include <wx/msgdlg.h>
#include <wx/bitmap.h>
#include <wx/imagpng.h>
#include <wx/gdicmn.h>
#include <wx/stdpaths.h>
#include <wx/platinfo.h>

#include <Poco/String.h>
#include <Poco/Format.h>
#include <Poco/Path.h>
#include <Poco/File.h>
#include <Poco/Timespan.h>
#include <Poco/Timestamp.h>
#include <Poco/DateTimeFormatter.h>
#include <Poco/Environment.h>

#include "gdev/utility/Strings.h"
#include "gdev/utility/osguard.h"

#include "AppImpl.h"

#include "gdev/appWx/App.h"

// Get build time value:
char __BUILD_EPOCH_GUARD__ = 0;
extern char __BUILD_EPOCH__;
#pragma weak __BUILD_EPOCH__ = __BUILD_EPOCH_GUARD__

#define _addImageHandler( type ) \
	if( wxImage::FindHandler( wxBITMAP_TYPE_##type ) == NULL ) { \
		wxImage::AddHandler( new wx##type##Handler() ); \
	}

namespace gdev {
namespace appWx {

App::App():
	gdev::utility::Loggable(getName())
{
	gdev::utility::Loggable::init();

	SetAppName(getName());

	AppImpl::initThreads();
}

App::~App() {
	log().information( "Shutdown!" );

	if(!singleInstanceGlobalLock.isNull()) {
		singleInstanceGlobalLock->unlock();
	}
}
bool App::OnInit() {
	try {
		AppImpl::initHandlerSigsegv();

		if(!initStrings()) {
			return false;
		}

		singleInstanceGlobalLock = new Poco::NamedMutex(getName());
		if(!singleInstanceGlobalLock->tryLock()) {
			singleInstanceGlobalLock = NULL;
			std::string msg = gdev::utility::Strings::getInstance()->getString("msgbox_error_another_instance_running");
			msg = Poco::format(msg.c_str(), getName());

			wxMessageBox(
					_fixStrWX(msg),
					_getStrWX("error"),
					wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
			);

			return false;
		}

		Poco::File(getDirSettings()).createDirectories();

		if(!isDebug()) {
			const std::string nameFileLog = Poco::format("%s.log", getName());
			gdev::utility::Loggable::init(Poco::Message::PRIO_INFORMATION, Poco::Path(getDirSettings(), nameFileLog).toString());
		}

		log().information("Application: '%s'", getName());
		log().information("By: '%s'", getAuthor());
		log().information("Version: '%s'", getVersion());
		log().information("Built on: '%s'", getBuildTime());
		log().information("Built for: '%s'", gdev::getBuildOS());
		log().information("Running on: '%s'", Poco::Environment::osDisplayName());

		gdev::utility::Strings::Ptr strPtr = gdev::utility::Strings::getInstance();
		log().information("Language set to: '%s'", strPtr->getLang());

		_addImageHandler(PNG);
		_addImageHandler(JPEG);
		_addImageHandler(BMP);

		return init();

	} catch(const Poco::Exception& e) {
		log().error(e.displayText());
	} catch(const std::exception& e) {
		log().error(std::string(e.what()));
	} catch(...) {
		log().error(std::string("Application encountered unknown error!"));
	}

	wxMessageBox(
			_getStrWX( "msgbox_error_init" ),
			_getStrWX( "error" ),
			wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
	);

	return false;
}

bool App::initStrings()
{
	try {
		gdev::utility::Strings::Ptr strPtr = gdev::utility::Strings::getInstance();
		std::vector<std::string> langs = strPtr->getLangs();

		std::string sysLanguage = _fixStrSTL(wxLocale::GetLanguageCanonicalName(wxLocale::GetSystemLanguage()));

		if(sysLanguage.empty()) {
			strPtr->setLang( langs.at(0) );
			return true;
		}

		sysLanguage = Poco::toUpper(sysLanguage.substr(0, 2));
		typedef std::vector<std::string>::iterator It;
		for(It it=langs.begin(), itEnd=langs.end(); it!=itEnd; it++) {
			if(!sysLanguage.compare(*it)) {
				strPtr->setLang(*it);
				return true;
			}
		}

		strPtr->setLang(langs.at(0));

		return true;

	} catch(const Poco::Exception& e) {
		log().error(e.displayText());
	} catch(const std::exception& e) {
		log().error(std::string(e.what()));
	} catch(...) {
		log().error(std::string("Application encountered unknown error!"));
	}

	return false;
}

bool App::OnExceptionInMainLoop()
{
	const std::string errFormat = "Exception in main loop: '%s'";
	try {
		throw;

	} catch(Poco::Exception& e) {
		log().error(errFormat.c_str(), e.displayText());
	} catch(std::exception& e) {
		log().error(errFormat.c_str(), std::string(e.what()));
	} catch(...) {
		log().error(errFormat.c_str(), std::string("Application encountered unknown error!"));
	}

	wxMessageBox(
			_getStrWX("msgbox_error_main_loop_exception"),
			_getStrWX("fatal"),
			wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
	);

	return true;
}

void App::OnFatalException()
{
	log().fatal("SIGSEGV - Shutdown!");

	wxMessageBox(
			_getStrWX("msgbox_error_sigsegv"),
			_getStrWX("fatal"),
			wxOK|wxICON_ERROR|wxSTAY_ON_TOP|wxCENTER
	);
}

std::string App::getDirSettings() {
	const std::string nameDirSettings = Poco::format(".%s", getName());
	return Poco::Path(Poco::Path::home(), nameDirSettings).toString();
}

std::string App::getBuildTime(const std::string& fmt) {
	if(&::__BUILD_EPOCH__ == &::__BUILD_EPOCH_GUARD__) {
		return "<build-time-unknown>";
	}
	Poco::Timestamp bt(((Poco::Timestamp::TimeDiff)&::__BUILD_EPOCH__) * Poco::Timespan::SECONDS);
	return Poco::DateTimeFormatter::format(bt, fmt, Poco::DateTimeFormatter::UTC);
}

}
}
