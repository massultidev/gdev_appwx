/* Copyright by: P.J. Grochowski */

#include <wx/app.h>
#include <wx/utils.h>

#include "gdev/utility/osguard.h"

#include "AppImpl.h"

#if _IS_LINUX
	#include <X11/Xlib.h>
#endif

#if _IS_WINDOWS
	::LONG WINAPI handleSEH(::EXCEPTION_POINTERS*) {
		wxApp::GetInstance()->OnFatalException();
		::exit(1);
	}
#endif

namespace gdev {
namespace appWx {

void AppImpl::initHandlerSigsegv() {
	#if _IS_WINDOWS
		::SetUnhandledExceptionFilter(::handleSEH);
	#else
		wxHandleFatalExceptions(true);
	#endif
}

void AppImpl::initThreads() {
	#if _IS_LINUX
		XInitThreads();
	#endif
}

}
}
