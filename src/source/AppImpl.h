/* Copyright by: P.J. Grochowski */

#ifndef SRC_SOURCE_APPIMPL_H_
#define SRC_SOURCE_APPIMPL_H_

namespace gdev {
namespace appWx {

class AppImpl
{
	public:
		static void initHandlerSigsegv();
		static void initThreads();
};

}
}

#endif
