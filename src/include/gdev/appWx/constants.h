/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_APPWX_CONSTANTS_H_
#define SRC_INCLUDE_GDEV_APPWX_CONSTANTS_H_

/*
 * Purpose of this header is to provide single,
 * unified access point for externally set defines.
 */

#define DEFINE_MISSING "<define-missing>"

//-----------------------------------------
#ifndef AUTHOR
	#define AUTHOR DEFINE_MISSING
#endif

//-----------------------------------------
#ifndef APP_NAME
	#define APP_NAME DEFINE_MISSING
#endif

//-----------------------------------------
#ifndef APP_VER
	#define APP_VER DEFINE_MISSING
#endif

#endif
