/* Copyright by: P.J. Grochowski */

#ifndef SRC_INCLUDE_GDEV_APPWX_APP_H_
#define SRC_INCLUDE_GDEV_APPWX_APP_H_

#include <string>

#include <wx/setup.h>
#include <wx/app.h>

#include <Poco/SharedPtr.h>
#include <Poco/NamedMutex.h>

#include "gdev/utility/osguard.h"
#include "gdev/utility/Loggable.h"
#include "gdev/appWx/constants.h"

namespace gdev {
namespace appWx {

class App:
	public wxApp,
	public gdev::utility::Loggable
{
	public:
		App();
		virtual ~App();

		virtual bool init() = 0;

		bool OnInit();
		bool OnExceptionInMainLoop();
		void OnFatalException();

		static inline bool isDebug() {
			#ifdef GDEV_DEBUG
				return true;
			#else
				return false;
			#endif
		}

		static inline std::string getAuthor() {
			return std::string(AUTHOR);
		}
		static inline std::string getName() {
			return std::string(APP_NAME);
		}
		static inline std::string getVersion() {
			return std::string(APP_VER);
		}
		static std::string getBuildTime(const std::string& fmt = "%Y.%m.%d-%H:%M");
		static std::string getDirSettings();

	private:
		bool initStrings();

		Poco::SharedPtr<Poco::NamedMutex> singleInstanceGlobalLock;
};

}
}

#endif
